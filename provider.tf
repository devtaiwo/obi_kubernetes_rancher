terraform {
  required_providers {
    helmfile = {
      source = "mumoshu/helmfile"
      version = "0.14.1"
    }
    docker = {
       source = "kreuzwerker/docker"
       version = "2.15.0"
    }
  }
}


provider "docker" {
  host = "unix:///var/run/docker.sock"
}

provider "kubernetes" {
  config_path    = "./kubeconfig.yaml"
  config_context = "default"
} 
